# LuaTask_V3.0

#### Description
基于合宙2G(RDA8955/F)平台的GSM通信模组Lua编程框架，串口和网络接口(socket,websocket,mqtt,http)支持同步回调和异步回调，同步支持全双工和中断消息机制。基于V3.0框架写一个透传 DTU 代码，只需要4行代码：
-- 创建 uart 对象
local u1 = uart.new(1, 115200)
-- 创建 webSocket 对象
local sc = socket.new("180.97.80.55", "12415", "tcp")
sys.taskInit(sc.start, sc, 180, nil, nil, 25, "heart", function(msg)u1:send(msg) end)
sys.taskInit(u1.start, u1, nil, 10, nil,  function(msg)sc:send(msg) end)

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
