--[[
Author: 稀饭放姜
Date: 2020-12-18 15:50:05
LastEditors: 稀饭放姜
LastEditTime: 2021-03-12 18:13:19
FilePath: \LuaTask_V3.0\Luat_To_One\drivers\sensor.lua
--]]
require "utils"
module(..., package.seeall)
-- 初始化并打开I2C操作
-- @number I2C 内部ID
-- @return number I2C的速率
local function i2c_open(id, speed)
    if i2c.setup(id, speed or i2c.SLOW) ~= i2c.SLOW then
        i2c.close(id)
        return i2c.setup(id, speed or i2c.SLOW)
    end
    return i2c.SLOW
end

--- 读取寄存器值
-- @number id： I2C端口号
-- @number addr:从机地址
-- @number reg: 寄存器地址
-- @return number: 寄存器当前值
function readRegister(id, addr, reg)
    log.info("-------->I2C OPEN Result:", i2c_open(id))
    i2c.send(id, addr, reg)
    local _, val = pack.unpack(i2c.recv(id, addr, 1), 'b')
    i2c.close(id)
    log.info("读取", string.format("寄存器:0x%02x 当前值:0x%02x", reg, val or 0))
    return val;
end

--- 写寄存器方法
-- @number  id: I2C端口号
-- @number addr:从机地址
-- @number reg: 寄存器地址
-- @number ...: 要写入寄存器其他值
-- @return number:  寄存当前值
function writeRegister(id, addr, reg, ...)
    log.info("-------->I2C OPEN Result:", i2c_open(id))
    i2c.send(id, addr, {reg, ...})
    local _, val = pack.unpack(i2c.recv(id, addr, 1), 'b')
    log.info("重读", string.format("寄存器:0x%02x 当前值:0x%02x", reg, val or 0))
    i2c.close(id)
    return val
end

-------------------------------------- KXTJ3-1057 驱动代码开始 --------------------------------------
local KXTJ3_ADDR = 0x18 --KXTJ3 的地址
local KXTJ3_DCST_RESP = 0x0C -- KXTJ3 的数字通自检寄存器
local KXTJ3_SELF_TEST = 0x3A -- KXTJ3 的功自检寄存器
-- 加速度输出寄存器
local KXTJ3_XOUT_L = 0x06
local KXTJ3_XOUT_H = 0x07
local KXTJ3_YOUT_L = 0x08
local KXTJ3_YOUT_H = 0x09
local KXTJ3_ZOUT_L = 0x0A
local KXTJ3_ZOUT_H = 0x0B
-- 中断寄存器
local KXTJ3_INT_SOURCE1 = 0x16
local KXTJ3_INT_SOURCE2 = 0x17
local KXTJ3_STATUS_REG = 0x18
local KXTJ3_INT_REL = 0x1A
-- 控制寄存器
local KXTJ3_CTRL_REG1 = 0x1B -- KXTJ3的CTRL_REG1地址
local KXTJ3_CTRL_REG2 = 0x1D
local KXTJ3_INT_CTRL_REG1 = 0x1E
local KXTJ3_INT_CTRL_REG2 = 0x1F
local KXTJ3_DATA_CTRL_REG = 0x21
local KXTJ3_WAKEUP_COUNTER = 0x29
local KXTJ3_WAKEUP_THRESHOLD_H = 0x6A
local KXTJ3_WAKEUP_THRESHOLD_L = 0x6B

--- 数字通信自检验证
-- number id: I2C端口号
-- return number 正常返回0x55,其他值为异常
function dcst(id)
    local val = readRegister(id, KXTJ3_ADDR, KXTJ3_DCST_RESP)
    log.info("KXTJ3C DCST Result:", string.format("0x%02x", val or 0))
    return val;
end

--- 读中断状态
-- number id: I2C端口号
-- return number 返回状态寄存器当前值
function readStatus(id)
    local val = readRegister(id, KXTJ3_ADDR, KXTJ3_STATUS_REG)
    log.info("KXTJ3C read interrupt status:", string.format("0x%02x", val or 0))
    if val == 0x10 then
        readInterrupt(id, 1)
    end
    return val;
end

--- 清中断状态
-- number id: I2C端口号
-- return nil
function clearStatus(id)
    readRegister(id, KXTJ3_ADDR, KXTJ3_INT_REL)
    log.info("Clear Interrupt status register：", "OK")
end

--- 读取中断源寄存器
-- number id: I2C端口号
-- @number src: 1 读中断源1寄存器, 2读中断源2寄存器
-- @return number: 返中断源寄存器的值
function readInterrupt(id, src)
    local val = 0
    if src == 2 then
        val = readRegister(id, KXTJ3_ADDR, KXTJ3_INT_SOURCE2)
        log.info("read INT_SOURCE2 register：", string.format("0x%02x", val or 0))
    else
        val = readRegister(id, KXTJ3_ADDR, KXTJ3_INT_SOURCE1)
        log.info("read INT_SOURCE1 register：", string.format("0x%02x", val or 0))
    end
    return val
end

--- 配置 KXTJ3工作模式
-- number id: I2C端口号
-- @number mode: 0 准备模式, 1工作模式
-- @return number: 返中断源寄存器的值
function setMode(id, mode)
    log.info("-------->I2C OPEN Result:", i2c_open(id))
    i2c.send(id, KXTJ3_ADDR, KXTJ3_CTRL_REG1)
    local _, val = pack.unpack(i2c.recv(id, KXTJ3_ADDR, 1), 'b')
    i2c.send(id, KXTJ3_ADDR, {KXTJ3_CTRL_REG1, mode == 0 and bit.clear(val or 0, 7) or bit.set(val or 0, 7)})
    val = readRegister(id, KXTJ3_ADDR, KXTJ3_CTRL_REG1)
    i2c.close(id)
    log.info("读取CTRL_REG1寄存器:", string.format("当前值:0x%02x", val or 0))
end

--- 读取三轴输出值,注意结果是 Tri-axis 数字量
-- @char  axis: 'x','y','z' 分别表示x,y,z轴
-- @number n: 分辨率,可选值8,12,14(CTRL_REG1配置)
-- @return number: Tri-axis Digital
function readAxis(id, axis, n)
    local val, recv, reg = 0, {}, {}
    if axis == 'x' then
        reg[1] = KXTJ3_XOUT_L
        reg[2] = KXTJ3_XOUT_H
    elseif axis == 'y' then
        reg[1] = KXTJ3_YOUT_L
        reg[2] = KXTJ3_YOUT_H
    elseif axis == 'z' then
        reg[1] = KXTJ3_ZOUT_L
        reg[2] = KXTJ3_ZOUT_H
    else
        return 0
    end
    recv[1] = readRegister(id, KXTJ3_ADDR, reg[1])
    recv[2] = readRegister(id, KXTJ3_ADDR, reg[2])
    if recv[2] then
        val = recv[2] * 256 + recv[1]
        if n == 8 then
            return recv[2]
        elseif n == 12 then
            return (recv[2] > 0x7F) and bit.bor(bit.rshift(val, 4), 0xF000) or bit.band(bit.rshift(val, 4), 0x0FFF)
        elseif n == 14 then
            return (recv[2] > 0x7F) and bit.bor(bit.rshift(val, 2), 0xC000) or bit.band(bit.rshift(val, 2), 0x3FFF)
        end
    end
    return 0;
end

-- KXTJ3-1057 功自检
-- number id: I2C端口号
-- @return nil
function selfTest(id)
    setMode(id, 0)
    writeRegister(id, KXTJ3_ADDR, KXTJ3_SELF_TEST, 0xCA)
    setMode(id, 1)
    log.info("on self test axis-x: ", readAxis(id, 'x', 8))
    log.info("on self test axis-y: ", readAxis(id, 'y', 8))
    log.info("on self test axis-z: ", readAxis(id, 'z', 8))
    setMode(id, 0)
    writeRegister(id, KXTJ3_ADDR, KXTJ3_SELF_TEST, 0x00)
    setMode(id, 1)
    log.info("out self test axis-x: ", readAxis(id, 'x', 8))
    log.info("out self test axis-y: ", readAxis(id, 'y', 8))
    log.info("out self test axis-z: ", readAxis(id, 'z', 8))
end
-------------------------------------- KXTJ3-1057 驱动代码结束 --------------------------------------
--- 初始化配置
-- number id: I2C端口号
-- @return nil
function init(id)
    -- 进入配置模式
    setMode(id, 0)
    -- 复位控制寄存器2
    writeRegister(id, KXTJ3_ADDR, KXTJ3_CTRL_REG2, 0x86)
    -- 配置控制寄存器2 为50HZ
    writeRegister(id, KXTJ3_ADDR, KXTJ3_CTRL_REG2, 0x06)
    -- 配置唤醒延时和唤阈值
    writeRegister(id, KXTJ3_ADDR, KXTJ3_WAKEUP_COUNTER, 10)
    writeRegister(id, KXTJ3_ADDR, KXTJ3_WAKEUP_THRESHOLD_H, (500 - (500 % 256)) / 256)
    writeRegister(id, KXTJ3_ADDR, KXTJ3_WAKEUP_THRESHOLD_L, 500 % 256)
    writeRegister(id, KXTJ3_ADDR, KXTJ3_DATA_CTRL_REG, 0x02)
    -- 配置中断控制寄存器1
    -- bit3 IEL 中断锁存    0锁存, 1脉冲(0.05ms)
    -- bit4 IEA 有效电平    0激活时低电平, 1激活时高电平
    -- bit5 IEN 引脚使能    0关闭, 1开启
    writeRegister(id, KXTJ3_ADDR, KXTJ3_INT_CTRL_REG1, bit.set(0, 5))
    -- 配置中断控制寄存器2
    -- writeRegister(id, KXTJ3_ADDR, KXTJ3_INT_CTRL_REG2, bit.set(0, 0, 1, 2, 3, 4, 5))
    -- 配置控制寄存器1 配置唤中断,(B0010 0010)
    -- bit1 WUFE    运动唤醒检测    0关闭, 1开启
    -- bit5 DRDYE   数据有效中断,   0关闭, 1开启
    -- bit6 RES     KXTJ3性能模式,  0低功耗8位模式, 1高性能模式 12 or 14bit
    -- bit7 PC1     KXTJ3模式切换,  0配置模式, 1工作模式
    writeRegister(id, KXTJ3_ADDR, KXTJ3_CTRL_REG1, bit.set(0, 1, 7))
    -- 清中断
    clearStatus(id)
    log.info("KXTJ3C init done: ", "ok!")
end
