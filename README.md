# LuaTask_V3.0
合宙 2G/4G/CAT.1 通用 lib 库
#### 介绍
基于合宙 2G(RDA8955/F) 和 4G(ASR1802/S) 通信模组的 Lua 编程框架，串口和网络接口 (socket,websocket,mqtt,http) 支持同步回调和异步回调，同步支持全双工和中断消息机制。合宙称之为 Luat -- Luat = Lua + AT, Luat 推出的物联网开源架构，依托于通信模块做简易快捷的开发，将传统的AT命令用 Lua 语言封装成 API，并提供各种功能应用的 demo，大大减少用户的开发成本。

#### 软件架构
软件架构说明
LuaTask 是 实现Luat 开源架构的最新实现。利用底层中断(包括定时器)事件与消息队列机制,配合Lua的协程和函数式编程实现类上位机编程方式处理嵌入式开发。降低模块片上系统的开发机制。

#### 更新说明
从 V3.0.2 开始 socket 和 websocket 创建对象不使用地址,而connect方法必须使用地址
详见demo。

#### 开发进度
备注：未 done 的用的是LuaTask V2.0的库。

1. sys.lua  done
2. ril.lua  done
3. clib.lua done
4. db.lua   done
5. link.lua done
6. net.lua  done
7. sim.lua  done
8. socket.lua done
9. mqtt.lua done
10. http.lua done
11. fifo.lua done
12. misc.lua done
13. utils.lua done
14. common.lua done
15. console.lua done
16. dns.lua     done
17. netled.lua  done
18. ntp.lua     done
19. patch.lua   done
20. websocket.lua done

#### 使用说明
- 基于V3.0框架写一个透传 DTU 代码，只需要4行代码：
```
-- 创建 uart 对象
local u1 = uart.new(1, 115200)
-- 创建 webSocket 对象
local sc = socket.new("tcp")
sys.taskInit(sc.start, sc, "180.97.80.55", "12415", 180, nil, nil, 25, "heart", function(msg)u1:send(msg) end)
sys.taskInit(u1.start, u1, nil, 10, nil,  function(msg)sc:send(msg) end)
```
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
