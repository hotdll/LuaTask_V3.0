--- 模块功能：串口和SOCKET透传功能测试(TASK版)
-- @author openLuat
-- @module uart.testUartTask
-- @license MIT
-- @copyright openLuat
-- @release 2018.10.20
require "utils"
require "pm"
require "dns"
require "lbsLoc"
require "socket"
module(..., package.seeall)
-- 此处的IP和端口请填上你自己的socket服务器和端口
-- local host, port = "wiki.airm2m.com", "49001"
-- local host, port = "180.97.80.55", "12415"
local host, port = "io.yyia.top", "7777"
-- 创建 uart 对象
local u1 = uart.new(1, 921600)
-- 创建 webSocket 对象
local sc = socket.new("tcp")
-- 发送注册包
sc:on("open", function()sc:send("hello socket server!") end)
-- 启动DTU任务进程
sys.taskInit(sc.start, sc, host, port, 30, nil, nil, 0, nil, function(msg)
    log.info("uart 1 write msg:", msg:toHex())
    u1:send(msg)
end)
sys.taskInit(u1.start, u1, nil, 5, nil, function(msg)sc:send(msg) end)
-- 打印剩余内存
sys.timerLoopStart(function()
    log.info("打印占用的内存:", _G.collectgarbage("count"))-- 打印占用的RAM
    log.info("打印可用的空间", rtos.get_fs_free_size())-- 打印剩余FALSH，单位Byte
end, 10000)
