--- 模块功能：串口和socket透传功能测试(TASK版)
-- @author openLuat
-- @module uart.testUartTask
-- @license MIT
-- @copyright openLuat
-- @release 2018.10.20
require "utils"
require "pm"
require "dns"
require "socket"
module(..., package.seeall)
-- 此处的IP和端口请填上你自己的socket服务器和端口
local host, port = "io.yyia.top", "7777"
-- local host, port = "180.97.80.55", "12415"
-- 创建 uart 和 socket 对象
local u1 = uart.new(1, 115200)
local sc = socket.new("tcp")
sc:on("open", function()
    sc:send("hello socket server!")
end)
u1:on("recv", function(msg)
    log.info("收到串口发来的数据:", msg)
    sc:send(msg)
end)
sc:on("open", function()
    sc:send("hello socket server!")
end)
sc:on("recv", function(msg)
    log.info("收到 socket server 的消息:", msg)
    u1:send(msg)
end)
sc:on("sent", function()
    log.info("sent to socket:", "发送消息已完成!")
end)
sc:on("error", function(msg)
    log.error("socket error:", msg)
end)
sc:on("close", function(code)
    log.info("socket closed,关闭码:", code)
end)
-- 启动任务进程
sys.taskInit(sc.start, sc, host, port, 180)
sys.taskInit(u1.start, u1, nil, 10)
sys.timerLoopStart(function()
    log.info("打印占用的内存:", _G.collectgarbage("count"))-- 打印占用的RAM
    log.info("打印可用的空间", rtos.get_fs_free_size())-- 打印剩余FALSH，单位Byte
end, 10000)
