--必须在这个位置定义PROJECT和VERSION变量
--PROJECT：ascii string类型，可以随便定义，只要不使用,就行
--VERSION：ascii string类型，如果使用Luat物联云平台固件升级的功能，必须按照"X.X.X"定义，X表示1位数字；否则可随便定义
PROJECT = "LED"
VERSION = "3.0.0"
-- 配置uart:send方法是阻塞发送还是非阻塞发送
-- 非阻塞可能会产生分包。阻塞会阻塞luatVM
uart.BLOCK = false
--加载日志功能模块，并且设置日志输出等级
--如果关闭调用log模块接口输出的日志，等级设置为log.LOG_SILENT即可
require "log"
LOG_LEVEL = log.LOGLEVEL_TRACE

require "sys"
require "pins"
sys.taskInit(function()
    pmd.ldoset(7, pmd.LDO_VLCD)
    pmd.ldoset(7, pmd.LDO_VMMC)
    local led = pins.setup(3, 1)
    while true do
        led(1)
        sys.wait(500)
        led(0)
        sys.wait(500)
    end
end)
sys.timerLoopStart(function()
    log.info("打印占用的内存:", _G.collectgarbage("count"))-- 打印占用的RAM
    log.info("打印可用的空间", rtos.get_fs_free_size())-- 打印剩余FALSH，单位Byte
end, 1000)

--启动系统框架
sys.init(0, 0)
sys.run()
