--必须在这个位置定义PROJECT和VERSION变量
--PROJECT：ascii string类型，可以随便定义，只要不使用,就行
--VERSION：ascii string类型，如果使用Luat物联云平台固件升级的功能，必须按照"X.X.X"定义，X表示1位数字；否则可随便定义
PROJECT = "audio"
VERSION = "3.0.0"

--加载日志功能模块，并且设置日志输出等级
--如果关闭调用log模块接口输出的日志，等级设置为log.LOG_SILENT即可
require "log"
LOG_LEVEL = log.LOGLEVEL_TRACE

require "sys"
require "pins"
require "audio"

sys.taskInit(function()
    local status = "play"
    local co, index = coroutine.running(), 1
    local list = {
        "/ldata/pwron.mp3",
        "/ldata/sms.mp3",
        "/ldata/call.mp3",
    }
    sys.wait(10000)
    -- 开始播放
    pins.setup(2, function(msg)
        if msg == cpu.INT_GPIO_POSEDGE then
            if status == "stop" then
                status = "play"
                log.error("播放已恢复!")
                coroutine.resume(co)
            else
                status = "stop"
                log.error("播放已停止!")
                audio.stop()
            end
        end
    end, pio.PULLUP)
    pins.setup(6, function(msg)
        if msg == cpu.INT_GPIO_POSEDGE then
            status = "play"
            audio.stop()
            index = index - 2
            index = index > 0 and index or #list + index
            coroutine.resume(co)
        end
    end, pio.PULLUP)
    pins.setup(7, function(msg)
        if msg == cpu.INT_GPIO_POSEDGE then
            status = "play"
            audio.stop()
            coroutine.resume(co)
        end
    end, pio.PULLUP)
    
    while true do
        log.error("当前播放的:", index .. "." .. list[index])
        if audio.play("MP3", list[index]) == -1 then
            coroutine.yield()
        end
        index = index >= #list and 1 or index + 1
    end
end)

--启动系统框架
sys.init(0, 0)
sys.run()
