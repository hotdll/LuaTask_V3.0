--- 模块功能：DNS功能测试(TASK版)
-- @author openLuat
-- @module uart.testUartTask
-- @license MIT
-- @copyright openLuat
-- @release 2018.10.20
require "pm"
require "dns"
require "utils"
require "socket"
module(..., package.seeall)

-- 此处演示dns
sys.taskInit(function()
    while not socket.isReady() do sys.wait(2000) end
    log.info("iot-auth.cn-shanghai.aliyuncs.com -->ip:", dns.request("iot-auth.cn-shanghai.aliyuncs.com"))
    log.info("www.163.com -->ip:", dns.request("www.163.com"))
    log.info("www.qq.com -->ip:", dns.request("www.qq.com"))
    log.info("www.openluat.com -->ip:", dns.request("www.openluat.com"))
    log.info("www.yyia.org -->ip:", dns.request("www.yyia.org"))
    log.info("www.yyia.top -->ip:", dns.request("www.yyia.top"))
    log.info("基站定位的坐标:", lbsLoc.request())
end, ...)
