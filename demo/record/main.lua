--必须在这个位置定义PROJECT和VERSION变量
--PROJECT：ascii string类型，可以随便定义，只要不使用,就行
--VERSION：ascii string类型，如果使用Luat物联云平台固件升级的功能，必须按照"X.X.X"定义，X表示1位数字；否则可随便定义
PROJECT = "audio"
VERSION = "3.0.0"

--加载日志功能模块，并且设置日志输出等级
--如果关闭调用log模块接口输出的日志，等级设置为log.LOG_SILENT即可
require "log"
LOG_LEVEL = log.LOGLEVEL_TRACE

require "sys"
require "pins"
require "audio"

sys.taskInit(function()
    local status = "stop"
    local co, index = coroutine.running(), 0
    sys.wait(5000)
    -- 开始播放
    pins.setup(3, function(msg)
        if msg == cpu.INT_GPIO_POSEDGE then
            if status == "stop" then
                status = "play"
                log.error("播放已开始!")
                coroutine.resume(co)
            else
                log.error("播放已停止!")
                audio.stop("RECORD")
            end
        end
    end, pio.PULLUP)
    while true do
        log.error("回放已准备", "按3键可以开始回放")
        coroutine.yield()
        audio.play("RECORD", 0)
        status = "stop"
        log.error("回放已结束", "按键可重新开始回放")
    end
end)
sys.taskInit(function()
    local status = "stop"
    local co, index = coroutine.running(), 0
    sys.wait(5000)
    pins.setup(2, function(msg)
        if msg == cpu.INT_GPIO_POSEDGE then
            if status == "stop" then
                status = "play"
                log.error("录音已开始!")
                coroutine.resume(co)
            else
                log.error("录音已停止!")
                audio.stopRecord()
            end
        end
    end, pio.PULLUP)
    while true do
        log.error("录音已准备", "按2键可以开始录音")
        coroutine.yield()
        log.error("录音已开始", "按2键可以结束录音")
        audio.record(0, 50, 7)
        status = "stop"
        log.error("录音结束,录音的长度:", audio.getSize(0))
    end
end)

--启动系统框架
sys.init(0, 0)
sys.run()
