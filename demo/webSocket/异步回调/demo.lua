--- 模块功能：串口功能测试(TASK版)
-- @author openLuat
-- @module uart.testUartTask
-- @license MIT
-- @copyright openLuat
-- @release 2018.10.20
require "utils"
require "pm"
require "dns"
require "webSocket"
module(..., package.seeall)
-- 创建 uart 和 webSocket 对象
local url = "ws://121.40.165.18:8800"
local u1 = uart.new(1, 115200)
local ws = webSocket.new()
u1:on("recv", function(msg)
    log.info("收到串口发来的数据:", msg)
    ws:send(msg)
end)
ws:on("open", function()
    ws:send("hello webSocket server!")
end)
ws:on("message", function(msg)
    log.info("收到 webSocket server 的消息:", msg)
    u1:send(msg)
end)
ws:on("sent", function()
    log.info("sent to webSocket:", "发送消息已完成!")
end)
ws:on("error", function(msg)
    log.error("webSocket error:", msg)
end)
ws:on("close", function(code)
    log.info("webSocket closed,关闭码:", code)
end)
-- 启动任务进程
sys.taskInit(ws.start, ws, url, 180)
sys.taskInit(u1.start, u1, nil, 10)
sys.timerLoopStart(function()
    log.info("打印占用的内存:", _G.collectgarbage("count"))-- 打印占用的RAM
    log.info("打印可用的空间", rtos.get_fs_free_size())-- 打印剩余FALSH，单位Byte
end, 10000)
