module(..., package.seeall)

local gx,gy,gz,ax,ay,az,temp = 0,0,0,0,0,0,0

function rgb(r,g,b)
    return ((r-r%8)/8)*2048 + ((g-g%4)/4)*32 + ((b-b%8)/8)
end

disp.setbkcolor(rgb(139,126,102))

--屏幕宽高
local rw,rh = disp.getlcdinfo()
--球xy加速度、球位置
local bx,by,bxa,bya = rw/2,rh/2,0,0
--加速度系数
local aa = 2
--球图片大小
local ballSize = 25

function show()
    disp.clear()
    --log.info("",bx,by,bxa,bya)
    -- disp.puttext(string.format("temp:%.02f",temp),0,6*16)
    bxa,bya = bxa+ay*aa,bya+ax*aa--当前加速度+测量加速度
    --计算位移
    bx,by = bx-bxa,by+bya
    --边缘反弹计算
    if bx < 0 then bx = -bx/1.2 bxa = -bxa/1.2 end
    if by < 0 then by = -by/1.2 bya = -bya/1.2 end
    if bx > rw-ballSize then bx = rw-ballSize-(bx-(rw-ballSize))/1.2 bxa = -bxa/1.2 end
    if by > rh-ballSize then by = rh-ballSize-(by-(rh-ballSize))/1.2 bya = -bya/1.2 end
    disp.putimage("/lua/ball"..tostring(ballSize)..".png",bx,by)
    --disp.drawrect(bx,by,bx+ballSize-1,by+ballSize-1,rgb(0,0,0))
    disp.update()
end







local i2cslaveaddr = 0x68 --mpu6050
local i2cid = 0

if i2c.setup(i2cid,i2c.SLOW) ~= i2c.SLOW then
    log.error("testI2c.init","fail")
    return
end

sys.taskInit(function ()
    i2c.send(i2cid,i2cslaveaddr,{0X6b,0x80})--复位
    sys.wait(100)
    i2c.send(i2cid,i2cslaveaddr,{0X6b,0x00})--唤醒
    sys.wait(100)
    i2c.send(i2cid,i2cslaveaddr,{0x19,0x07})--陀螺仪采样率，典型值：0x07(125Hz)
    i2c.send(i2cid,i2cslaveaddr,{0x1A,0x06})--低通滤波频率，典型值：0x06(5Hz)
    i2c.send(i2cid,i2cslaveaddr,{0x1B,0x18})--陀螺仪自检及测量范围，典型值：0x18(不自检，2000deg/s)
    i2c.send(i2cid,i2cslaveaddr,{0x1C,0x01})--加速计自检、测量范围及高通滤波频率，典型值：0x01(不自检，2G，5Hz)
    i2c.send(i2cid,i2cslaveaddr,0x75)--读器件地址
    local revData = i2c.recv(i2cid,i2cslaveaddr,1)
    log.info("i2c read",revData:toHex())
    if revData:byte() == 0x68 then
        i2c.send(i2cid,i2cslaveaddr,{0x6b,0x01})--设置x轴的pll为参考
        i2c.send(i2cid,i2cslaveaddr,{0x6c,0x00})--加速度计与陀螺仪开启
    else
        log.info("i2c","address not right",revData:byte())
        while true do
            i2c.send(i2cid,i2cslaveaddr,0x75)--读器件地址
            local revData = i2c.recv(i2cid,i2cslaveaddr,1)
            log.info("i2c read",revData:toHex())
            if revData:byte() == 0x68 then break end
            sys.wait(200)
        end
        return
    end
    --处理接收到的数据，变成正确数值
    function getTrueData(d)
        i2c.send(i2cid,i2cslaveaddr,d)--获取的地址
        local s = i2c.recv(i2cid,i2cslaveaddr,2)--获取2字节
        local _,d = pack.unpack(s,">h")
        return d or 0
    end
    rtos.closeSoftDog()--关狗
    while true do
        --sys.wait(1)
        --gx = getTrueData(0x43)/16384
        --gy = getTrueData(0x45)/16384
        --gz = getTrueData(0x47)/16384
        --log.info("mpu6050.Gyroscope",gx,gy,gz)
        temp = getTrueData(0x41) / 340.0 + 36.53
        --log.info("mpu6050.temp",temp)
        ax = getTrueData(0x3b)/16384
        ay = getTrueData(0x3d)/16384
        --az = getTrueData(0x3f)/16384
        --log.info("mpu6050.Accelerometer",ax,ay,az)
        show()
    end
end)
