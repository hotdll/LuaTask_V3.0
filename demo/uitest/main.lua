--必须在这个位置定义PROJECT和VERSION变量
--PROJECT：ascii string类型，可以随便定义，只要不使用,就行
--VERSION：ascii string类型，如果使用Luat物联云平台固件升级的功能，必须按照"X.X.X"定义，X表示1位数字；否则可随便定义
PROJECT = "TEST"
VERSION = "1.0.1"

--根据固件判断模块类型
moduleType = string.find(rtos.get_version(),"8955") and 2 or 4

require "log"
LOG_LEVEL = log.LOGLEVEL_TRACE

require "sys"

require "misc"

require "net"

--require"color_lcd_spi_gc9106"
require"color_lcd_spi_st7735s"
--require"color_lcd_spi_st7735"
--require"color_lcd_spi_ili9341"
--require"mono_lcd_spi_ssd1306"

--require"keyTouch"
require"vcd"
--require"mpu6050"

--启动系统框架
sys.init(0, 0)
sys.run()
