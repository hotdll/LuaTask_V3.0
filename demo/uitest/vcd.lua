require"audio"
module(..., package.seeall)
local seed = tonumber(tostring(os.time()):reverse():sub(1, 7)) + rtos.tick()
function randomseed(val)
    seed = val
end
function random()
    local next = seed
    next = next * 1103515245
    next = next + 12345
    local result = (next / 65536) % 2048
    next = next * 1103515245
    next = next + 12345
    result = result * 2 ^ 10
    result = bit.bxor(result, (next / 65536) % 1024)
    next = next * 1103515245
    next = next + 12345
    result = result * 2 ^ 10
    result = bit.bxor(result, (next / 65536) % 1024)
    seed = next
    return result
end

--rgb值转rgb565
function rgb(r,g,b)
    return ((r-r%8)/8)*2048 + ((g-g%4)/4)*32 + ((b-b%8)/8)
end


local pw,ph = 30,50
local rw,rh = disp.getlcdinfo()
local w,h = rw-pw,rh-ph

local x,y = random()%w,random()%h
local dx,dy = 1,1
dx,dy = random()%2==0 and dx or -dx,random()%2==0 and dy or -dy

sys.taskInit(function ()
    rtos.closeSoftDog()--????
    --audio.play(0,"FILE","/lua/giligili.mp3",7,nil,true)
    while true do
        --log.info("info now",x,y,dx,dy)
        x=x+dx
        y=y+dy
        if x<=0 or y<=0 or x>=w or y>=h then
            local r,g,b = random()%100+155,random()%100+155,random()%100+155
            local ran = random()%3
            disp.setbkcolor(rgb(ran == 0 and 0 or r,ran == 1 and 0 or g,ran == 2 and 0 or b))
            if x<=0 or x>=w then dx = -dx x=x+dx end
            if y<=0 or y>=h then dy = -dy y=y+dy end
            sys.wait(1)
        end
        disp.clear()
        disp.putimage("/lua/vcd.png",x,y)
        disp.update()
        -- disp.clear()
        -- for i=0,w do
        --     for j=0,h do
        --         local r = random()%2 == 0 and 255 or 0
        --         local g = random()%2 == 0 and 255 or 0
        --         local b = random()%2 == 0 and 255 or 0
        --         disp.drawrect(i,j,i,j,rgb(r,g,b))
        --     end
        -- end
        -- disp.update()
        -- sys.wait(1)
    end
end)

