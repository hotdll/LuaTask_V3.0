--必须在这个位置定义PROJECT和VERSION变量
--PROJECT：ascii string类型，可以随便定义，只要不使用,就行
--VERSION：ascii string类型，如果使用Luat物联云平台固件升级的功能，必须按照"X.X.X"定义，X表示1位数字；否则可随便定义
PROJECT = "FIFO"
VERSION = "1.0.0"

--加载日志功能模块，并且设置日志输出等级
--如果关闭调用log模块接口输出的日志，等级设置为log.LOG_SILENT即可
require "log"
LOG_LEVEL = log.LOGLEVEL_TRACE
--[[
如果使用UART输出日志，打开这行注释的代码"--log.openTrace(true,1,115200)"即可，根据自己的需求修改此接口的参数
如果要彻底关闭脚本中的输出日志（包括调用log模块接口和Lua标准print接口输出的日志），执行log.openTrace(false,第二个参数跟调用openTrace接口打开日志的第二个参数相同)，例如：
1、没有调用过sys.opntrace配置日志输出端口或者最后一次是调用log.openTrace(true,nil,921600)配置日志输出端口，此时要关闭输出日志，直接调用log.openTrace(false)即可
2、最后一次是调用log.openTrace(true,1,115200)配置日志输出端口，此时要关闭输出日志，直接调用log.openTrace(false,1)即可
]]
--log.openTrace(true,1,115200)
require "sys"

-- require "net"
--每1分钟查询一次GSM信号强度
--每1分钟查询一次基站信息
-- net.startQueryAll(60000, 60000)
--加载控制台调试功能模块（此处代码配置的是uart1，波特率115200）
--此功能模块不是必须的，根据项目需求决定是否加载
--使用时注意：控制台使用的uart不要和其他功能使用的uart冲突
--使用说明参考demo/console下的《console功能使用说明.docx》
--require "console"
--console.setup(1, 115200)
--加载网络指示灯功能模块
--根据自己的项目需求和硬件配置决定：1、是否加载此功能模块；2、配置指示灯引脚
--合宙官方出售的Air800和Air801开发板上的指示灯引脚为pio.P0_28，其他开发板上的指示灯引脚为pio.P1_1
local is4gLod = rtos.get_version():upper():find("ASR1802")
-- require "netLed"
-- netLed.setup(true, is4gLod and 64 or 33)
--网络指示灯功能模块中，默认配置了各种工作状态下指示灯的闪烁规律，参考netLed.lua中ledBlinkTime配置的默认值
--如果默认值满足不了需求，此处调用netLed.updateBlinkTime去配置闪烁时长
--加载错误日志管理功能模块【强烈建议打开此功能】
--如下2行代码，只是简单的演示如何使用errDump功能，详情参考errDump的api
-- require "errDump"
-- errDump.request("udp://ota.airm2m.com:9072")
--加载远程升级功能模块【强烈建议打开此功能】
--如下3行代码，只是简单的演示如何使用update功能，详情参考update的api以及demo/update
--PRODUCT_KEY = "v32xEAKsGTIEQxtqgwCldp5aPlcnPs3K"
--require "update"
--update.request()
-- require "ntp"
-- ntp.timeSync(24, function()log.info(" AutoTimeSync is Done!") end)
-- require "demo"
require "fifo"
local str = string.rep("a", 1024)
local ver = rtos.get_version()
local lua = ver:find("8955") and "/ldata/" or "/lua/"
sys.taskInit(function()
    sys.wait(10000)
    -- local rom = fifo.newRom("/gps_msg")
    -- for i = 1, 10 do
    --     rom:push(str)
    --     sys.wait(50)
    -- end
    -- while true do
    --     local dat = rom:pop()
    --     if dat then
    --         log.info("离线存储的数据:", #dat)
    --     else
    --         break
    --     end
    --     sys.wait(50)
    -- end
    -- log.error("任务 1 已执行完成!")
    -- 这个示范本来应该每次读一行，结果一次完了。
    -- 样例脚本，每行1024字节(包括"\n")
    ---[[
    print("---------------- test 1 start ---------------")
    local file = io.open(lua .. "test.dat")
    for l in file:lines() do
        print("------", #l)
    end
    file:close()
    sys.wait(1000)
    file = io.open(lua .. "test.dat")
    while true do
        local l = file:read()
        if not l then break end
        print("----------2", #l)
    end
    file:close()
    sys.wait(1000)
    -- 这个示每次都一行1027个字节，只读到1026个字节，。
    -- 样例脚本，每行1027字节(包括"\n")
    print("---------------- test 2 start ---------------")
    file = io.open(lua .. "test2.dat")
    for l in file:lines() do
        print("#########", #l)
    end
    file:close()
    file = io.open(lua .. "test2.dat")
    while true do
        local l = file:read()
        if not l then break end
        print("##########2", #l)
    end
    file:close()
    sys.wait(1000)
    -- 这个示范本来应该每次读一行，结果正确。
    -- 样例脚本，每行1022字节(包括"\n")
    print("---------------- test 3 start ---------------")
    file = io.open(lua .. "test3.dat")
    while true do
        local l = file:read()
        if not l then break end
        print("**********1", #l)
    end
    file:close()
    sys.wait(1000)
    file = io.open(lua .. "test3.dat")
    for l in file:lines() do
        print("**********2", #l)
    end
    file:close()
--]]
end)
sys.timerLoopStart(function()
    log.info("打印占用的内存:", _G.collectgarbage("count"))-- 打印占用的RAM
    log.info("打印可用的空间", rtos.get_fs_free_size())-- 打印剩余FALSH，单位Byte
end, 1000)


--启动系统框架
sys.init(0, 0)
sys.run()
