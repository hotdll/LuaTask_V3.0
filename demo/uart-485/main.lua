--必须在这个位置定义PROJECT和VERSION变量
--PROJECT：ascii string类型，可以随便定义，只要不使用,就行
--VERSION：ascii string类型，如果使用Luat物联云平台固件升级的功能，必须按照"X.X.X"定义，X表示1位数字；否则可随便定义
PROJECT = "UART"
VERSION = "3.0.0"

--加载日志功能模块，并且设置日志输出等级
--如果关闭调用log模块接口输出的日志，等级设置为log.LOG_SILENT即可
require "log"
LOG_LEVEL = log.LOGLEVEL_TRACE

require "netLed"
if sys.is8910 then
    pmd.ldoset(15, pmd.LDO_VLCD)
    netLed.setup(true, 1, 4)
elseif sys.is4gLod then
    pmd.ldoset(7, pmd.LDO_VLCD)
    netLed.setup(true, 64, 65)
else
    netLed.setup(true, 33)
end

require "sys"
-- local u1 = uart.new(1, 115200)-- 非阻塞发送
local u1 = uart.new(1, 115200, 8, uart.PAR_NONE, uart.STOP_1, 19, 1, true)-- 阻塞发送
u1:on("sent", function()
    -- log.info("-------------------------------------")
    end)
local count = 0
sys.taskInit(u1.start, u1, nil, 5, true, function(msg)
    count = count + #msg
    log.info("uart read length:",count)
    u1:send(msg)
end)
sys.timerLoopStart(function()
    log.info("打印占用的内存:", _G.collectgarbage("count"))-- 打印占用的RAM
    log.info("打印可用的空间", rtos.get_fs_free_size())-- 打印剩余FALSH，单位Byte
end, 1000)

--启动系统框架
sys.init(0, 0)
sys.run()
