--必须在这个位置定义PROJECT和VERSION变量
--PROJECT：ascii string类型，可以随便定义，只要不使用,就行
--VERSION：ascii string类型，如果使用Luat物联云平台固件升级的功能，必须按照"X.X.X"定义，X表示1位数字；否则可随便定义
PROJECT = "GPSV3"
VERSION = "2.0.0"
PRODUCT_KEY = "DPVrZXiffhEUBeHOUwOKTlESam3aXvnR"
--加载日志功能模块，并且设置日志输出等级
--如果关闭调用log模块接口输出的日志，等级设置为log.LOG_SILENT即可
require "log"
LOG_LEVEL = log.LOGLEVEL_TRACE
--[[
如果使用UART输出日志，打开这行注释的代码"--log.openTrace(true,1,115200)"即可，根据自己的需求修改此接口的参数
如果要彻底关闭脚本中的输出日志（包括调用log模块接口和Lua标准print接口输出的日志），执行log.openTrace(false,第二个参数跟调用openTrace接口打开日志的第二个参数相同)，例如：
1、没有调用过sys.opntrace配置日志输出端口或者最后一次是调用log.openTrace(true,nil,921600)配置日志输出端口，此时要关闭输出日志，直接调用log.openTrace(false)即可
2、最后一次是调用log.openTrace(true,1,115200)配置日志输出端口，此时要关闭输出日志，直接调用log.openTrace(false,1)即可
]]
--log.openTrace(true,1,115200)
require "sys"

require "net"
--每1分钟查询一次GSM信号强度
--每1分钟查询一次基站信息
net.startQueryAll(60000, 60000)

--加载控制台调试功能模块（此处代码配置的是uart1，波特率115200）
--此功能模块不是必须的，根据项目需求决定是否加载
--使用时注意：控制台使用的uart不要和其他功能使用的uart冲突
--使用说明参考demo/console下的《console功能使用说明.docx》
--require "console"
--console.setup(1, 115200)
--加载硬件看门狗功能模块
--根据自己的硬件配置决定：1、是否加载此功能模块；2、配置Luat模块复位单片机引脚和互相喂狗引脚
--合宙官方出售的Air201开发板上有硬件看门狗，所以使用官方Air201开发板时，必须加载此功能模块
--[[
require "wdt"
wdt.setup(pio.P0_30, pio.P0_31)
]]
--加载网络指示灯功能模块
--根据自己的项目需求和硬件配置决定：1、是否加载此功能模块；2、配置指示灯引脚
--合宙官方出售的Air800和Air801开发板上的指示灯引脚为pio.P0_28，其他开发板上的指示灯引脚为pio.P1_1
require "netLed"
if sys.is8910 then
    pmd.ldoset(15, pmd.LDO_VLCD)
    netLed.setup(true, 1, 4)
elseif sys.is4gLod then
    pmd.ldoset(7, pmd.LDO_VLCD)
    netLed.setup(true, 64, 65)
else
    netLed.setup(true, 33)
end
--网络指示灯功能模块中，默认配置了各种工作状态下指示灯的闪烁规律，参考netLed.lua中ledBlinkTime配置的默认值
--如果默认值满足不了需求，此处调用netLed.updateBlinkTime去配置闪烁时长
--加载错误日志管理功能模块【强烈建议打开此功能】
--如下2行代码，只是简单的演示如何使用errDump功能，详情参考errDump的api
require "errDump"
errDump.request("udp://ota.airm2m.com:9072")

--加载远程升级功能模块【强烈建议打开此功能】
--如下3行代码，只是简单的演示如何使用update功能，详情参考update的api以及demo/update
--PRODUCT_KEY = "v32xEAKsGTIEQxtqgwCldp5aPlcnPs3K"
--require "update"
--update.request()
--加载串口功能测试模块（串口1，TASK方式实现，串口帧超时分帧）
require "gps"
gps.open(sys.is8910 and 3 or 2, 9600, 10, false, true)
require "sim"
sys.taskInit(function()
    sys.wait(5000)
    if (not sim.getIccid()) or (sim.getIccid() == "") then
        sim.setId(sim.getId() == 0 and 1 or 0, function(result)
            if result then sys.restart("simcross") end
        end)
    elseif sim.getId() == 0 then
        sim.setId(1, function(result)
            if result then sys.restart("simcross") end
        end)
    end
end)


sys.timerLoopStart(function()
    log.info("打印占用的内存:", _G.collectgarbage("count"))-- 打印占用的RAM
    log.info("打印可用的空间", rtos.get_fs_free_size())-- 打印剩余FALSH，单位Byte
end, 5000)

require "color_lcd_spi_st7735S"
--LCD分辨率的宽度和高度(单位是像素)
local WIDTH, HEIGHT, BPP = disp.getlcdinfo()
--1个ASCII字符宽度为8像素，高度为16像素；汉字宽度和高度都为16像素
local CHAR_WIDTH = 8

--[[
函数名：getxpos
功能  ：计算字符串居中显示的X坐标
参数  ：
str：string类型，要显示的字符串
返回值：X坐标
]]
function getxpos(str)
    return (WIDTH - string.len(str) * CHAR_WIDTH) / 2
end

function setcolor(color)
    if BPP ~= 1 then return disp.setcolor(color) end
end

sys.subscribe("GPS_MSG_REPORT", function(...)
    log.info("MSG:", unpack(arg))
    log.info("MSG-INT:", gps.isFix(), gps.getIntLocation())
    log.info("MSG-gcj02:", gps.isFix(), gps.getGcj02())
    log.info("MSG-gcbd09:", gps.isFix(), gps.getBd09())

    --清空LCD显示缓冲区
    disp.clear()
    disp.setbkcolor(0x0000)
    disp.puttext(common.utf8ToGb2312("定位模式"), getxpos(common.utf8ToGb2312("定位模式")), 0)
    local tm = misc.getClock()
    local datestr = string.format("%04d", tm.year) .. "-" .. string.format("%02d", tm.month) .. "-" .. string.format("%02d", tm.day)
    local timestr = string.format("%02d", tm.hour) .. ":" .. string.format("%02d", tm.min) .. ":" .. string.format("%02d", tm.sec)
    --显示日期
    disp.setcolor(0x07E0)
    disp.puttext(datestr, 0, 24)
    --显示时间
    disp.setcolor(0x001F)
    disp.puttext(timestr, 88, 24)
    disp.setcolor(0xFFE0)
    local lng, lat = gps.getBd09()
    disp.puttext(common.utf8ToGb2312("经度: ") .. lng, 0, 44)
    disp.puttext(common.utf8ToGb2312("纬度: ") .. lat, 0, 64)
    --刷新LCD显示缓冲区到LCD屏幕上
    disp.update()
end)

require "sensor"
sys.taskInit(function()
    pins.setup(14, 1, pio.PULLUP)
    -- pins.setup(15, nil, pio.PULLUP)
    sys.wait(3000)
    while true do
        sensor.dcst(2)
        sensor.selfTest(2)
        sensor.init(2)
        sys.wait(100)
    end
end, ...)

--启动系统框架
sys.init(0, 0)
sys.run()
